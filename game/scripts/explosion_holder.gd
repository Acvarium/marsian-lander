extends Node2D
var i = 0
var end_ex = true
var lines = []

func _ready():
	randomize()
#	$exploder.add_line_vectors(Vector2(0,0), Vector2(100,100))
#	$exploder._draw()
	pass 
	
func explode():
	lines.clear()
	i = 0
	for j in range(20):
		var l = rand_range(1, 20)
		var v = Vector2(1,0).rotated(randf() * PI*2) * l
		lines.append(v)
	end_ex = false

func _physics_process(delta):
	if end_ex:
		$exploder.clear_lines()
		$exploder.update()
		return
	$exploder.clear_lines()
	for l in lines:
		$exploder.add_line_vectors(l * i * 0.2, l * i)
	$exploder.update()
	i += 1.5
	if i > 20:
		end_ex = true
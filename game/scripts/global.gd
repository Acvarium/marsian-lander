extends Node
var game_seed = 0
var fuel = 1000
var flat_places = []
var end_plane = 0

func _ready():
	randomize()
	game_seed = randi()
	for i in range(5):
		var dist = rand_range(200, 1000)
		flat_places.append(dist)
		end_plane += dist

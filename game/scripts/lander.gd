extends Node2D
var velocity = Vector2(2,0)
var rot_dir = 0
var acc_dir = 0
var collide = false
var coll = null
var main_node = null
var impact_moment = 0.0
var down_pressed = false
var rot_speed = 0.03

func _ready():
	main_node = get_node("/root/main")
	coll = $coliders
	$fire/fire_anim.play("fire")
	$fire.add_line_vectors(Vector2(0,0), Vector2(0,30))
	$fire.add_line_vectors(Vector2(-5,0), Vector2(-5,20))
	$fire.add_line_vectors(Vector2(5,0), Vector2(5,20))
	$fire._draw()
	
	var s = 13
	$ship.add_line_vectors(Vector2(0,-s), Vector2(s,s))
	$ship.add_line_vectors(Vector2(0,-s), Vector2(-s,s))
	$ship.add_line_vectors(Vector2(s,s), Vector2(-s,s))
	$ship._draw()


func _physics_process(delta):
	if impact_moment < 1:
		if down_pressed:
			var loop_rot = rotation + PI
			loop_rot = loop_rot - (int(loop_rot / (PI*2)) * PI * 2) - PI
			rotation = loop_rot
			if abs(rotation) < 0.1:
				rotation = 0
			else:
				if rotation > 0:
					rotation -= rot_speed
				else:
					rotation += rot_speed
					
		if acc_dir > 0:
			if global.fuel > 0:
				global.fuel -= 0.5
			else:
				global.fuel = 0
				$fire.visible = false
				acc_dir = 0
			main_node.update_fuel()
		var pos = position
		velocity.y += 0.008
		if rot_dir != 0:
			rotation += rot_dir * rot_speed
		var rot_vec = Vector2(acc_dir * 0.015, 0).rotated(rotation + PI/2)
		velocity -= rot_vec
		for c in $coliders.get_children():
			if main_node.is_point_collide(c.global_position) > 0:
				get_impact()
				if velocity.y > 0:
					velocity.y = 0
				
				velocity.x = 0
		pos += velocity
		position = pos
		main_node.follow_cam(pos)
	else:
		if OS.get_ticks_msec() - impact_moment > 3000:
			get_tree().reload_current_scene()
			


func get_impact():
	var impact = velocity.length()
	if (impact > 0.31):
		$ship.visible = false
		$fire.visible = false
		$explosion_holder.visible = true
		$explosion_holder.explode()
		impact_moment = OS.get_ticks_msec()

func _input(event):
	if impact_moment < 1:
		if Input.is_action_just_pressed("ui_up"):
			$fire.visible = true
			acc_dir = 1
		elif Input.is_action_just_released("ui_up"):
			acc_dir = 0
			$fire.visible = false
	
		if Input.is_action_just_pressed("ui_down"):
			down_pressed  = true
			rot_dir = 0
		
		elif Input.is_action_just_released("ui_down"):
			down_pressed  = false
	
		if Input.is_action_just_pressed("ui_left") and not down_pressed:
			rot_dir = -1
		if Input.is_action_just_released("ui_left") and rot_dir == -1:
			rot_dir = 0
		if Input.is_action_just_pressed("ui_right") and not down_pressed:
			rot_dir = 1
		if Input.is_action_just_released("ui_right") and rot_dir == 1:
			rot_dir = 0
		
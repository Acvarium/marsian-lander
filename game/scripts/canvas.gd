extends Node2D

var lines = []
var a = Vector2(0,1)

func _ready():
	pass # Replace with function body.

func clear_lines():
	lines.clear()
	
func add_line_points(point_a_x, point_a_y, point_b_x, point_b_y):
	lines.append([Vector2(point_a_x, point_a_y), Vector2(point_b_x, point_b_y)])

func add_line_vectors(point_a, point_b):
	lines.append([point_a, point_b])

func add_line(line):
	lines.append(line)

func _draw():
	for l in lines:
    	draw_line(l[0], l[1], Color.white, 2)

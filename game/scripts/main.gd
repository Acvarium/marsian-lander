extends Node2D
var screen_size = Vector2(1024, 600)
var canvas = null
var landscape = []
var noise = OpenSimplexNoise.new()
var stars = []
var star_num = 50
var camera
var move_dir = 0
var zoom_dir = 0

func update_fuel():
	$camera/fuel.text = str(int(global.fuel))

func _ready():
	randomize()
	$camera/fuel.text = str(int(global.fuel))
	if global.fuel <= 0:
		global.game_seed = randi()
		global.fuel = 1000
	camera = $camera
	var _seed = global.game_seed
	noise.seed = _seed
	noise.octaves = 5
	noise.period = 300.0
	noise.persistence = 0.5
	canvas = $canvas
	create_landscape()
	star_gen()
	redraw()

func get_landscape_point(var x):
	var y = noise.get_noise_2d(x, 1.0) * 300 + 430
	return y

func is_point_collide(point):
	var land_y = get_landscape_point(point.x) 
#	var land_y = noise.get_noise_2d(point.x, 1.0) * 300 + 430
	return point.y - land_y

func follow_cam(point):
	if point.x > (camera.position.x + 0.75 * screen_size.x):
		camera.position.x += (point.x - (camera.position.x + 0.75 * screen_size.x)) * 0.3
		redraw()
	if point.x < (camera.position.x + 0.25 * screen_size.x):
		camera.position.x += (point.x - (camera.position.x + 0.25 * screen_size.x)) * 0.3
		redraw()
	
func _input(event):
	return
	if Input.is_action_just_pressed("fire"):
		var _seed = randi()
		noise.seed = _seed
		create_landscape()
		redraw()
		
	if Input.is_action_just_pressed("ui_left"):
		move_dir = -1
	elif Input.is_action_just_pressed("ui_right"):
		move_dir = 1
	if Input.is_action_just_released("ui_left") and move_dir == -1:
		move_dir = 0
	if Input.is_action_just_released("ui_right") and move_dir == 1:
		move_dir = 0
	
	if Input.is_action_just_pressed("ui_up"):
		zoom_dir = -1
	elif Input.is_action_just_pressed("ui_down"):
		zoom_dir = 1
	if Input.is_action_just_released("ui_up") and zoom_dir == -1:
		zoom_dir = 0
	if Input.is_action_just_released("ui_down") and zoom_dir == 1:
		zoom_dir = 0

func star_gen():
	stars.clear()
	for i in range(star_num):
		var star = Vector2(rand_range(0, screen_size.x), rand_range(0,screen_size.y))
		var star2 = star
		star2.x += screen_size.x
		var star3 = star
		star3.x -= screen_size.x
		stars.append(star)
		stars.append(star2)
		stars.append(star3)

func create_landscape():
	landscape.clear()
	var x = 0
	var y = 350
	while x < screen_size.x * 1.3:
		y = get_landscape_point(x)
		var point = Vector2(x,y)
		landscape.append(y)
		x += 1

func redraw():
	canvas.clear_lines()
	var x_offset = camera.position.x 
	var extra = 100

	for x in range(screen_size.x + extra * 2):
		var y = get_landscape_point(x + x_offset - extra)
		var y2 = get_landscape_point(x + 1 + x_offset - extra)
		
		canvas.add_line_vectors(Vector2(x + x_offset - extra, y), Vector2(x + 1 + x_offset - extra, y2))
	var loop_x = fmod(x_offset, screen_size.x)

	var p = 0
	p = (int(camera.position.x) / int(global.end_plane)) * global.end_plane
	for j in range(2):
		for plane in global.flat_places:
			p += plane
			var y = get_landscape_point(p)
			canvas.add_line_vectors(Vector2(p, y), Vector2(p, y+50))

	for i in range(stars.size()):
		var star_pos = stars[i]
		star_pos.x = star_pos.x + x_offset - fmod(x_offset * 0.4, screen_size.x)
		var noise_point = get_landscape_point(star_pos.x)
		if (stars[i].y < noise_point):
			var point_shift = star_pos
			point_shift.y += 2
			canvas.add_line_vectors(star_pos, point_shift)
	canvas.update()

func _physics_process(delta):
	var pos = camera.position
	pos.x += move_dir * 3
	camera.position = pos
	if (move_dir != 0):
		redraw()
	if (zoom_dir != 0):
		var zoom = camera.zoom.x
		if not(zoom_dir == 1 and zoom > 1):
			zoom += zoom_dir * 0.01
			camera.zoom = Vector2(zoom, zoom)
	$camera/d/level.rotation = $lander.rotation
	$camera/d/vel_dir.rotation = $lander.velocity.normalized().angle() + PI/2
	var power = ($lander.velocity.length() / 2) * 100
	$camera/d/vel_power.value = power
	